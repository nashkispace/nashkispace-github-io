# ikhsan.gitlab.io

[![pipeline status](https://gitlab.com/ikhsan/ikhsan.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/ikhsan/ikhsan.gitlab.io/-/commits/master)

This is the source code for my personal website and blog [ikhsan.gitlab.io](https://ikhsan.gitlab.io).

## Tech stack and features
- React
- Next.js (SSG)
- Typescript
- EsLint
- Prettier
- Prism
- Styled-components
- Markdown
- RSS
- i18n

## Development
```bash
npm install
npm run dev
```
Then open [http://localhost:3000](http://localhost:3000).

## Deployment
Everything is deployed automatically thanks to Gitlab CI/CD and `.gitlab-ci.yml`!
